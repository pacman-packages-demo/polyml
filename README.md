# Poly/ML

Standard ML compiler originally written in the experimental language Poly. https://polyml.org

## Unofficial documentation
* [repology](https://repology.org/project/polyml/versions)
* WikipediA
  * [*Standard ML*](https://en.wikipedia.org/wiki/Standard_ML)
* [*Standard ML and how I’m compiling it*
  ](https://thebreakfastpost.com/2015/06/10/standard-ml-and-how-im-compiling-it/)
  2015 Chris Cannam

## Projects using neighbour languages
* ML
  * Standard ML
    * MLton
      * [pacman-packages-demo/smlnj](https://gitlab.com/pacman-packages-demo/smlnj)
    * SML/NJ
      * [pacman-packages-demo/smlnj](https://gitlab.com/pacman-packages-demo/smlnj)
  * OCaml
    * [pacman-packages-demo/ocaml](https://gitlab.com/pacman-packages-demo/ocaml)
    * [ocaml-packages-demo](https://gitlab.com/ocaml-packages-demo)
* Mercury
    * [apt-packages-demo/mercury](https://gitlab.com/apt-packages-demo/mercury)
